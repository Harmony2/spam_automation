from pynput.keyboard import Key, Controller
import time

keyboard = Controller()

def spam_name():
    try:
        name = input("what name will we spam?\n")
        times = int(input("how many times?\n"))

    except:
        print("Invalid format. Please try again.")
        return False

    print("[...] Initiating spam protocol")    
    for i in range(3, 0, -1):
        print(i)
        time.sleep(1)

    for i in range(0, times):
        keyboard.press(Key.shift_l)
        keyboard.press("2")
        
        keyboard.release(Key.shift_l)
        keyboard.release("2")

        for i in name:
            keyboard.press(i)
            keyboard.release(i)
        
        keyboard.press(Key.enter)
        keyboard.release(Key.enter)

        keyboard.press(Key.enter)
        keyboard.release(Key.enter)

        time.sleep(0.2)

    return True

spam_name()